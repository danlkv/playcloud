## Playground for cloud services

Simulate distributed computing

## Installation

```
pip install -e .
```

## Usage

Run playcloud on file:
```
playcloud file.plcl
```

Run playcloud in interactive mode (WIP):
```
playcloud
```

## File syntax

```
# This is a comment
send 5 write key 1 value 100
send 5 read key 1

kill node 1

# Send 10 read requests with keys rotating 1..5
send 10 read key 1-5

# Send 10 read requests keys and values rotating
send 10 write key 1-3 value 1-6

# Restore connection
resurrect node 1
```

## Example files:

- `./test/test_files/simple_test.plcl` simple test with killing and resurrecting nodes.
- `./test/test_files/test_disconnect.plcl` Test with single network partitioning into 2- and 3-node parts.
- `./test/test_files/fast_repartition_outage.plcl` Test with changing network partitions. A timestep-based quorum needed.
