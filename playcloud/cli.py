import fire
from . import log
from . import play

def cli_main(
    file=None,
):
    log("Welcome to PlayCloud!")
    if file is None:
        # interactive mode
        log("No file specified. Interactive mode not supported yet.")
        return

    with open(file, "r") as f:
        program = f.read()
    play(program)

def cli():
    fire.Fire(cli_main)
