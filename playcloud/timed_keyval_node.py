from .cloud import KeyValNode
from .logging import log, log_ctx

class TimedKeyValNode(KeyValNode):
    def get(self, key):
        log(f"KeyValNode[{self.name}].get", key, level='debug')
        return self._store.get(key, (0, None))

    def set(self, key, ts_value):
        ts, value = ts_value
        if ts == 0: # default value
            exist_ts, _ = self._store.get(key, (0, None))
            ts = 1 + exist_ts
            ts_value = (ts, value)
        log(f"KeyValNode[{self.name}].set", key, ts_value)
        self._store[key] = ts_value
        return ts

    def majority_vote_reconcile(self, cluster_value_votes, key):
        _rconn = len(cluster_value_votes)
        if _rconn < self.majority_size:
            raise Exception(f"Number of received reads ({_rconn}) is insufficent for read consensus")
        # find the value with latest timestamp
        log(f"KeyValNode[{self.name}].read: cluster_value_votes", cluster_value_votes, level='debug')
        sorted_values = sorted(cluster_value_votes.values(), key=lambda x: x[0], reverse=True)
        last_ts_value = sorted_values[0]

        log(f"KeyValNode[{self.name}].read: sorted ts_values", sorted_values, level='debug')

        # -- write to stale nodes
        for addr, _value in cluster_value_votes.items():
            if _value != last_ts_value:
                self._set_other(addr, key, last_ts_value)
        _, value = last_ts_value
        return value


    def write(self, key, value):
        with log_ctx(f"KeyValNode[{self.name}].write", key, value, level='debug'):
            # this is client API, so no ts is known, use default
            ts_value = (0, value)
            self_ts = self.set(key, ts_value)
            other_tss = {self._addr_sentinel: self_ts}
            for addr in self.cluster:
                try:
                    _ts = self.net.request(addr, ("set", key, ts_value))
                    other_tss[addr] = _ts
                except Exception as e:
                    log(f"KeyValNode[{self.name}].write: Error at addr [{addr}]: {e}. Skipping...")
                    continue

            if len(other_tss) < self.majority_size:
                raise Exception("Not enough nodes to reach write consensus")

            # -- reconcile last ts
            all_tss = list(other_tss.values())
            log(f"KeyValNode[{self.name}].write: all_tss", all_tss, level='debug')
            last_ts = max(all_tss)
            last_ts_value = (last_ts, value)
            # -- write to stale nodes
            for addr, _ts in other_tss.items():
                if _ts != last_ts:
                    self._set_other(addr, key, last_ts_value)

