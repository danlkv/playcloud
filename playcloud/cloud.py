from numpy import random
import numpy as np

from . import log, log_ctx, log_decor

class Node:
    def endpoint(self, request):
        raise NotImplementedError("Node.request")

class LoadBalancer(Node):
    def __init__(self, net, addrs):
        self.addrs = addrs
        self.net = net
        self.ix = 0

    def add_addr(self, addr):
        self.addrs.append(addr)

    def endpoint(self, request):
        n_tries = 0
        while True:
            addr = self.addrs[self.ix]
            self.ix = (self.ix + 1) % len(self.addrs)
            try:
                result = self.net.request(addr, request)
                return result
            except Exception as e:
                n_tries += 1
                if n_tries >= len(self.addrs):
                    raise e
                else:
                    log(f"LoadBalancer: Error at addr [{addr}]: {e}. Retrying...")
                    continue

class Network:
    def __init__(self, name):
        self.name = name
        self.nodes = {}
        self.lb = LoadBalancer(self, [])
        self.dead_nodes = set()
        self.dead_links = set()

    def __repr__(self):
        return f"{self.name}{self.addrs}"
    @property
    def addrs(self):
        return list(self.nodes.keys())

    def add_node(self, addr, node):
        if addr in self.addrs:
            raise Exception("Address already in use")

        for addrc, nodec in self.nodes.items():
            nodec.add_neighbor(addr)
            node.add_neighbor(addrc)
        self.nodes[addr] = node
        self.lb.add_addr(addr)

    def get_node(self, addr):
        return self.nodes[addr]


    @log_decor("Network.request")
    def request(self, addr, request, source=None):
        node = self.get_node(addr)
        if node in self.dead_nodes:
            raise Exception(f"Node {node.name} is dead")

        if source is not None:
            if tuple(sorted((source, addr))) in self.dead_links:
                raise Exception(f"Link {source}--{addr} is dead")
        response = node.endpoint(request)
        return response

    @log_decor("Net.request_load_balance", level='info')
    def request_load_balance(self, request):
        response = self.lb.endpoint(request)
        return response

    def kill(self, node_ix:int):
        """ Kill node """
        node = self.get_node(self.addrs[node_ix])
        log(f"Net.kill ix[{node_ix}] node[{node.name}]")
        self.dead_nodes.add(node)

    def resurrect(self, node_ix):
        """ Resurrect node """
        node = self.get_node(self.addrs[node_ix])
        log(f"Net.resurrect ix[{node_ix}] node[{node.name}]")
        self.dead_nodes.remove(node)

    def disconnect(self, source, dest):
        link = tuple(sorted((source, dest)))
        self.dead_links.add(link)

    def reconnect(self, source, dest):
        link = tuple(sorted((source, dest)))
        if link in self.dead_links:
            self.dead_links.remove(link)
        

class Netwrapper(Network):
    def __init__(self, node_addr, net:Network):
        self.addr = node_addr
        self.net = net

    def request(self, addr, request):
        return self.net.request(addr, request, source=self.addr)

class KeyValNode(Node):
    _addr_sentinel = -1
    def __init__(self, name, net:Network):
        self.name = name
        self.net  = net
        self._store = {}
        self.cluster = []

    def add_neighbor(self, addr):
        self.cluster.append(addr)

    def endpoint(self, request):
        log(f"KeyValNode[{self.name}].request", request, level='trace')
        req_type, *args = request
        if req_type == "read":
            return self.read(*args)
        elif req_type == "write":
            return self.write(*args)
        if req_type == "get":
            return self.get(*args)
        elif req_type == "set":
            return self.set(*args)

    def get(self, key):
        log(f"KeyValNode[{self.name}].get", key, level='debug')
        return self._store.get(key, None)

    @property
    def majority_size(self):
        return np.ceil((len(self.cluster) + 1 ) / 2)

    def read(self, key):
        with log_ctx(f"KeyValNode[{self.name}].read", key, level='debug'):
            value_self = self.get(key)
            check_size = min(4, len(self.cluster))
            # -- get all votes
            cluster_value_votes = {self._addr_sentinel:value_self}
            rand_addrs = random.choice(self.cluster, size=check_size, replace=False)
            for addr in rand_addrs:
                try:
                    _value = self.net.request(addr, ("get", key))
                    cluster_value_votes[addr] = _value
                except Exception as e:
                    log(f"KeyValNode[{self.name}].read: Error at addr [{addr}]: {e}. Skipping...")
                    continue

            # -- majority vote
            value = self.majority_vote_reconcile(cluster_value_votes, key)
        return value

    def majority_vote_reconcile(self, cluster_value_votes, key):
        if len(cluster_value_votes) == 1:
            raise Exception("Not enough nodes to reach read consensus")
        value_counts = {}
        for value in cluster_value_votes.values():
            value_counts[value] = value_counts.get(value, 0) + 1
        log(f"KeyValNode[{self.name}].read: value_counts", value_counts, level='debug')
        value, count = max(value_counts.items(), key=lambda x: x[1])
        log(self.cluster, count)
        if count < self.majority_size:
            raise Exception("No majority")

        # -- write to dissenter nodes
        for addr, _value in cluster_value_votes.items():
            if _value != value:
                self._set_other(addr, key, value)
        return value

    def _set_other(self, addr, key, value):
        if addr == self._addr_sentinel:
            log(
                f"KeyValNode[{self.name}].read: Update stale to self",
                f"{key}<-{value}"
                , level='debug')
            self.set(key, value)
        else:
            log(
                f"KeyValNode[{self.name}].read: Update stale to addr",
                f"{addr} {key}<-{value}"
                , level='debug')
            self.net.request(addr, ("set", key, value))

    def set(self, key, value):
        log(f"KeyValNode[{self.name}].set", key, value)
        self._store[key] = value

    def write(self, key, value):
        with log_ctx(f"KeyValNode[{self.name}].write", key, value, level='debug'):
            self._store[key] = value
            writes = 1
            for addr in self.cluster:
                try:
                    self.net.request(addr, ("set", key, value))
                    writes += 1
                except Exception as e:
                    log(f"KeyValNode[{self.name}].write: Error at addr [{addr}]: {e}. Skipping...")
                    continue

        log(self.majority_size, writes)
        if writes < self.majority_size:
            raise Exception("Not enough nodes to reach write consensus")
