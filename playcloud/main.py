import re
import itertools
from dataclasses import dataclass
from numpy import random
from . import log
from .cloud import Network, KeyValNode, Netwrapper
from .timed_keyval_node import TimedKeyValNode

# -- Parsing

COMMANDS = {}

def parse_line(line):
    regex = "^(?P<cmd>[a-z]+) (?P<args>.+)$"
    match = re.match(regex, line)
    if match is None:
        raise ValueError("Invalid line: " + line)
    cmd = match.group("cmd")
    args = match.group("args")
    if cmd not in COMMANDS:
        raise ValueError("Invalid command: " + cmd)
    return cmd, args

def parse_program(program):
    for line in program.splitlines():
        if line.startswith("#"):
            continue
        if line == "":
            continue
        yield parse_line(line)

# -- Execution

# ---- Requests

@dataclass
class ReadRequest:
    keys: list

    def __iter__(self):
        i = 0
        while True:
            yield (self.keys[i], )
            i = (i + 1) % len(self.keys)

def parse_range(vrange):
    values = vrange.split("-")
    if len(values) > 2:
        raise ValueError("Invalid range: " + vrange)
    if len(values) == 1:
        return [int(values[0])]
    start = int(values[0])
    end = int(values[1])
    return list(range(start, end + 1))

def parse_read_request(args):
    regex = "^(key) (?P<range>.+)$"
    match = re.match(regex, args)
    if match is None:
        raise ValueError("Invalid read request args: " + args)
    vrange = match.group("range")
    params = parse_range(vrange)
    return ReadRequest(params)

@dataclass
class WriteRequest:
    keys: list
    values: list

    def __iter__(self):
        i = 0
        j = 0
        while True:
            yield self.keys[i], self.values[j]
            i = (i + 1) % len(self.keys)
            j = (j + 1) % len(self.values)

def parse_write_request(args):
    regex = "^(key) (?P<value_key>.+) (value) (?P<value_value>.+)?$"
    match = re.match(regex, args)

    if match is None:
        raise ValueError("Invalid write request args: " + args)
    kval = match.group("value_key")
    RAND_SIZE = 10
    if kval == "random":
        value_key = list(random.randint(0, 1000, size=RAND_SIZE))
    else:
        value_key = parse_range(kval)
    vval = match.group("value_value")
    if vval is None:
        value_value = "random"
    else:
        value_value = vval

    if value_value == "random":
        value_value = list(random.randint(0, 1000, size=RAND_SIZE))
    else:
        value_value = parse_range(vval)

    return WriteRequest(value_key, value_value)

# ---- Functions

REQUEST_PARSERS = {
    "read": parse_read_request,
    "write": parse_write_request,
}

# ---- API
class Manager:
    def __init__(self, net:Network):
        self.net = net
        self.storage_ref = {}

    def _read_store(self, key):
        return self.storage_ref.get(key, None)

    def _write_store(self, key, value):
        self.storage_ref[key] = value

    def _ref_request(self, request):
        req_type = request[0]
        if req_type == "read":
            key = request[1]
            return self._read_store(key)
        elif req_type == "write":
            key, value = request[1:]
            self._write_store(key, value)

    def send(self, request):
        result = self.net.request_load_balance(request)
        ref = self._ref_request(request)
        if result != ref:
            log(f"main.send: Error: {result} != {ref}")
            raise ValueError("main.send: Error: {result} != {ref}")

    def kill(self, node):
        self.net.kill(node)

    def resurrect(self, node):
        self.net.resurrect(node)

def send(mgr:Manager, count, req_type, type_args):
    log("Sending", count, req_type, type_args)
    parser = REQUEST_PARSERS[req_type]
    request = iter(parser(type_args))
    for i in range(count):
        args = next(request)
        mgr.send([req_type, *args])


# ---- Commands
def send_cmd(mgr, args):
    # -- parse cmd args
    regex = r"^(?P<count>\d+) (?P<type>[a-z]+) (?P<type_args>.+)$"
    match = re.match(regex, args)
    if match is None:
        raise ValueError("Invalid send command args: " + args)
    count = int(match.group("count"))
    type = match.group("type")
    type_args = match.group("type_args")
    # -- execute cmd
    send(mgr, count, type, type_args)


COMMANDS["send"] = send_cmd

def kill_cmd(mgr, args):
    regex = r"^node (?P<node>\d+)$"
    match = re.match(regex, args)
    if match is None:
        raise ValueError("Invalid kill command args: " + args)
    node = int(match.group("node"))
    mgr.kill(node)

COMMANDS["kill"] = kill_cmd

def resurrect_cmd(mgr, args):
    regex = r"^node (?P<node>\d+)$"
    match = re.match(regex, args)
    if match is None:
        raise ValueError("Invalid resurrect command args: " + args)
    node = int(match.group("node"))
    mgr.resurrect(node)

COMMANDS["resurrect"] = resurrect_cmd

def disconnect_cmd(mgr, args):
    regex = r"^(?P<source_range>[\d-]+) (?P<target_range>[\d-]+)$"
    match = re.match(regex, args)
    if match is None:
        raise ValueError("Invalid disconnect command args: " + args)
    source_range = parse_range(match.group("source_range"))
    target_range = parse_range(match.group("target_range"))
    links = list(itertools.product(source_range, target_range))
    for source, target in links:
        mgr.net.disconnect(source, target)

COMMANDS["disconnect"] = disconnect_cmd

def reconnect_cmd(mgr, args):
    regex = r"^(?P<source_range>[\d-]+) (?P<target_range>[\d-]+)$"
    match = re.match(regex, args)
    if match is None:
        raise ValueError("Invalid reconnect command args: " + args)
    source_range = parse_range(match.group("source_range"))
    target_range = parse_range(match.group("target_range"))
    links = list(itertools.product(source_range, target_range))
    for source, target in links:
        mgr.net.reconnect(source, target)
    
COMMANDS["reconnect"] = reconnect_cmd


def play(program):
    log("Playing program:")
    net = Network("kvnet")
    K = 5
    node_addrs = list(range(K))
    for addr in node_addrs:
        netnode = Netwrapper(addr, net)
        node = TimedKeyValNode(addr, netnode)
        net.add_node(addr, node)

    mgr = Manager(net)

    commands = parse_program(program)

    for cmd in commands:
        log(cmd)
        COMMANDS[cmd[0]](mgr, cmd[1])
    final_data = mgr.storage_ref
    log(f"Final client data: {final_data}")
    for node in net.nodes:
        log(f"Final node {node} data: {net.nodes[node]._store}")

