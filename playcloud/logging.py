from contextlib import contextmanager
from functools import wraps
from loguru import logger
import sys

PREFIX = ""
DEPTH = 0
logger.remove()
def format(record):
    # -- module name
    mname_len = 20
    mname = record["name"]
    if len(mname) > mname_len:
        mname = mname[:mname_len-3] + "..."
    mname = mname.ljust(mname_len)
    record["name"] = mname

    # -- function name
    fname_len = 10
    fname = record["function"]
    if len(fname) > fname_len:
        fname = fname[:fname_len-3] + "..."
    fname = fname.ljust(fname_len)
    record["function"] = fname

    format="<green>{time:mm:ss.SSS}</green> | <level>{level: <6}</level> | <cyan>{name:<20}</cyan>:<cyan>{function:<10}</cyan>:<cyan>{line:<5}</cyan>\t {extra[plcl_prefix]}<level>{message}</level>\n"

    return format

logger.add(
    sys.stdout,
    format=format,
)
def log(*args, level='info', open=False, close=False, **kwargs):
    global PREFIX
    global DEPTH
    if open:
        PREFIX = " │" * DEPTH
        DEPTH += 1
        PREFIX += "─┬"
        
    elif close:
        DEPTH -= 1
        PREFIX = " │" * DEPTH
        PREFIX += " ╰" 
    else:
        PREFIX = " │" * (DEPTH - 1)
        if DEPTH > 0:
            PREFIX += " ├╴"

    level = level.upper()
    global logger
    logger = logger.bind(plcl_prefix=PREFIX)
    stack_depth = kwargs.pop('stack_depth', 1)
    logger.opt(depth=stack_depth).log(level, " ".join(map(str, args)), **kwargs)

@contextmanager
def log_ctx(*args, **kwargs):
    log(*args, open=True, **kwargs, stack_depth=3)
    try:
        yield
    except Exception as e:
        log(f"Error: {e}", level='error', close=True, stack_depth=2)
        raise
    log(*args, close=True, **kwargs, stack_depth=3)

def log_decor(*args, **kwargs):
    def decorator(func):
        @wraps(func)
        def wrapper(*args_f, **kwargs_f):
            log(*(args+args_f), open=True, **kwargs, stack_depth=2)
            try:
                res = func(*args_f, **kwargs_f)
            except Exception as e:
                log(f"Error: {e}", level='error', close=True, stack_depth=2)
                raise
            log(f"Result={res}", close=True, **kwargs)
            return res
        return wrapper
    return decorator
